Prevent updates to the cell text when cell execution of the same cell has commenced or completed.
